import numpy as np
import matplotlib.pyplot as plt
import skimage
import time
from scipy import optimize
import logging

from radarTools import MimoProcessor


class CalSettings():
    def __init__(self, metric="Variance", calMode="Phase", maxTimeCompensation=10e-6):
        self.metric = metric
        self.calMode = calMode
        self.maxTimeCompensation = maxTimeCompensation


def entropy(data):
    """
    Calculates image entropy

    Parameters:
    -----------
    data : np.ndarray
            The dataset on which to calculate entropy
    """
    squaredMagData = np.square(np.abs(data))

    energy = np.sum(squaredMagData)

    return -np.sum(np.multiply(squaredMagData, np.log(squaredMagData))/energy) + np.log(energy)


def supervariance(data, angularAxis=-1):
    """
    Calculates the variance of the image with weighted signal energy per range bin

    Parameters:
    -----------
    data : np.ndarray
            The dataset on which to calculate entropy
    """
    d = np.abs(data)

    # Mean of all values
    avg = np.mean(d)

    squaredMagData = np.square(d)

    # Energy for each range bin
    energyPerRangeBin = np.sum(squaredMagData, axis=angularAxis)
    avgEnergyPerRangeBin = np.mean(energyPerRangeBin)

    # Total signal energy
    energy = np.sum(squaredMagData)

    weightedNormVar = np.sum(np.square(
        ((d.transpose()-avg)*(energyPerRangeBin/avgEnergyPerRangeBin)).transpose()))/energy

    return -weightedNormVar


def variance(data):
    """
    Calculates the variance of the image

    Parameters:
    -----------
    data : np.ndarray
            The dataset on which to calculate entropy
    """
    d = np.abs(data)
    avg = np.mean(d)

    squaredMagData = np.square(d)
    energy = np.sum(squaredMagData)

    normvar = np.sum(np.square(d-avg))/energy

    return -normvar


class AutoCal():
    """
    Provides automatic calibration of radar images
    """

    def __init__(self, acqSettings, procSettings, calSettings, startVector=None, phaseAxis=-1):
        """
        Auto calibration initialization

        Do not use the local Calibrator object for actual compensation. Its compensation matrix shape is likely incorrect, if using a subset of the raw data for calibration.

        Parameters:
        -----------
        acqSettings : AcquisitionSettings object
        Stores relevant acquisition information

        procSettings : ProcessingSettings object
        Stores relevant processing information

        startVector : np.ndarray
        Optional argument for specifying starting phase of the global minimum search. Useful for tracking purposes

        phaseAxis : int
        Index of the dimension that stores data in angular direction
        """

        self.acqSettings = acqSettings
        self.procSettings = procSettings
        self.calSettings = calSettings
        self.time = 0

        self.logger = logging.getLogger("general_logs.AutoCalibrator")

        # In case of recording the history of calibrations, store them in this list
        self.compensationVectors = []

        # Enforce rectangular window, so that ends of vector are not masked
        self.procSettings.angularWindowType = "Rectangular"
        self.mimoProcessor = MimoProcessor(self.acqSettings, self.procSettings)

        self.bounds = []

        self.optimizer = None
        self.calMode = self.calSettings.calMode
        self.metric = self.calSettings.metric

        if self.calMode == "Time":
            self.calibrator = TimeCalibrator(
                self.acqSettings, self.calSettings)

            # Specify bounds for optimization
            for _ in range(self.acqSettings.V):
                self.bounds.append((0, 1))

        elif self.calMode == "Phase":
            # Specify bounds for optimization
            for _ in range(self.acqSettings.V):
                self.bounds.append((0, np.pi))

            self.calibrator = PhaseCalibrator(self.calSettings)

            # Storage for metric across iteration steps
        self.es = []

        if startVector is None:
            self.compensationVector = np.ones(
                self.acqSettings.V) * np.min(self.bounds)
        else:
            self.compensationVector = startVector

        self.currentCompensatedImage = None

    def run(self, rangeData, calFrequencies=None, history=False, linearComp=True):
        """
        Execute optimization for a certain compensation target

        Parameters:
        -----------
        rangeData : np.ndarray
        Range compressed data. No angular processing is allowed

        calFrequencies : double
        Frequency values for lower/higher end of calibration interval. This is only required for time based calibration
        """

        self.optimizationHistory = []

        self.rangeData = rangeData
        self.calFrequencies = calFrequencies

        self.calibrator.learnShape(self.rangeData)

        self.optimizer = optimize.minimize(
            self.__metric__, self.compensationVector, bounds=self.bounds, tol=1e-5)

        pV = self.optimizer.x

        # Remove linear term and absolute offset
        if linearComp:
            x = np.arange(len(pV))
            fit = np.polyfit(x, pV, 1)

            self.compensationVector = pV - fit[1] - fit[0]*x

        else:
            self.compensationVector = pV

        self.logger.info("Compensation values:")

        for c in self.compensationVector:
            if self.calMode == "Time":
                self.logger.info("%1.6f ns" % (c*100e-6*1e9))

            elif self.calMode == "Phase":
                self.logger.info("%1.6f" % c)

        if history:
            self.compensationVectors.append(self.compensationVector)
            self.compensationVectorAvg = np.mean(
                self.compensationVectors, axis=0)

        self.es = np.array(self.es)
        self.optimizationHistory = np.array(self.optimizationHistory)

    def __compensate__(self, compensationVector):
        """
        Compensate radar data with current phase vector. Yields radar range data, as well as image

        Parameters:
        -----------
        compensationVector : np.ndarray
                Current phase vector for correction that is evaluated
        """
        if self.calMode == "Time":
            self.calibrator.generateCompensation(
                compensationVector, self.calFrequencies)

        elif self.calMode == "Phase":
            if self.calFrequencies is not None:
                Warning(
                    "The calFrequencies parameter is not required for phase calibration.")

            self.calibrator.generateCompensation(compensationVector)

        self.currentCompensatedRangeData = self.calibrator.compensate(
            self.rangeData)

        self.currentCompensatedImage = self.mimoProcessor.calcAngularSpectrum(
            self.currentCompensatedRangeData)

    def __metric__(self, compVector):
        """
        Calculates the chosen metric for the image with current correction applied
        """
        self.__compensate__(compVector)

        if self.metric == "Entropy":
            ref = time.time()
            e = entropy(self.currentCompensatedImage)
            self.time += time.time()-ref

            self.es.append(e)
            self.optimizationHistory.append(compVector)
            return e

        elif self.metric == "Variance":
            ref = time.time()
            e = variance(self.currentCompensatedImage)
            self.time += time.time()-ref

            self.es.append(e)
            self.optimizationHistory.append(compVector)
            return e

        elif self.metric == "Supervariance":
            ref = time.time()
            e = supervariance(self.currentCompensatedImage)
            self.time += time.time()-ref

            self.es.append(e)
            self.optimizationHistory.append(compVector)
            return e

        else:
            raise TypeError("Metric type %s unknown" % self.metric)


class __Calibrator__():
    def __init__(self, calSettings):
        '''
        Calibrator initialization

        Parameters:
        -----------
        originalcompensationVector : np.ndarray
                Stores initial compensation parameters that are never changed inside here

        '''
        self.calSettings = calSettings
        self.compensationVector = None
        self.compensation = None

        try:
            self.originalcompensationVector = self.calSettings.originalcompensationVector
        except AttributeError:
            pass

        self.size = None
        self.antennas = None
        self.samples = None

    def learnShape(self, d):
        """
        Extract the shape of the dataset that needs to be compensated.

        Parameters:
        -----------
        d : np.ndarray or tuple
                Data in the shape of the actual radar data OR
                tuple of the data shape
        """
        try:
            # This works for np.ndarray type inputs
            self.size = d.shape
        except AttributeError:
            # Fallback, direct storage of the input shape
            self.size = d

        try:
            self.antennas, self.hops, self.samples = self.size
        except ValueError:  # No hopping measurements
            self.antennas, self.samples = self.size

    def rememberOriginal(self, original):
        """
        Copies the selected compensation parameters to a local field

        Parameters:
        -----------
        original : np.ndarray
                Phase vector to copy
        """
        self.calSettings.originalcompensationVector = np.copy(original)

    def compensate(self, data, compensation=None):
        '''
        High level compensation function. Calls the low-level function and converts input data to a suitable format

        Parameters:
        -----------
        data : np.ndarray or list
                The input radar data

        compensation : np.ndarray
                The compensation matrix (default is the compensation matrix that is locally stored in the calibrator)
        '''

        if compensation is None:
            # Allow use of external calibration data
            compensation = self.compensation

        if isinstance(data, list):
            # Convert to numpy array for compensation to work
            data = np.array(data)

        try:
            return self.__compensate__(data, compensation)
        except ValueError:
            return None

    def resetCal(self):
        self.compensation = None

    def getCalibrationState(self):
        return self.compensation != None

    def __compensate__(self, data, compensation):
        '''
        Low level compensation function. Simply multiplies the input data with compensation matrix (element-wise)

        Parameters:
        -----------
        data : np.ndarray
                The input radar data

        compensation : np.ndarray
                The compensation matrix
        '''
        return np.multiply(data, compensation)

    def generateCompensation(self, *args, **kwargs):
        raise NotImplementedError


class PhaseCalibrator(__Calibrator__):
    '''
    Calibration class for fixed phase calibration
    '''

    def __init__(self, calSettings):
        super().__init__(calSettings)
        # System settings currently unused

        self.logger = logging.getLogger("general_logs.PhaseCalibrator")

    def __generatePhaseTerm__(self, samples, calParams):
        """
        Generate phase term for radar signal compensation

        Parameters:
        -----------
        samples : int
                Amount of samples in a range spectrum

        calParams : tuple
                A set of calibration parameters
        """
        corrPhase = np.ones(samples, dtype=complex)

        try:
            _, angCor, _ = calParams
        except TypeError:
            angCor = calParams

        return np.exp(1j * angCor) * corrPhase

    def generateCompensation(self, compensationVector):
        """
        Calculates compensation matrix from a set of parameters

        Parameters:
        -----------
        compensationVector : list of tuples
                The parameters from which to calculate the calibration matrix
        """
        self.compensationVector = compensationVector

        compensation = np.zeros(self.size, dtype=complex)

        for compIndex, comp in enumerate(self.compensationVector):
            compensation[compIndex, :] = self.__generatePhaseTerm__(
                self.samples, comp)

        self.compensation = compensation

        return self.compensation

    def applyRotation(self, rotation):
        """
        Updates calibration vector with a rotated version of the original

        Parameters:
        -----------
        rotation : double
                The rotation value in radians
        """

        self.rotation = rotation

        # Generate the new phase vector
        rotatedcompensationVector = self.calSettings.originalcompensationVector + \
            np.arange(len(self.calSettings.originalcompensationVector)) * \
            self.rotation * 2 * np.pi
        self.generateCompensation(rotatedcompensationVector)


class TimeCalibrator(__Calibrator__):
    '''
    Calibration class for linear phase calibration.
    '''

    def __init__(self, calSettings):
        super().__init__(calSettings)
        self.logger = logging.getLogger("general_logs.TimeCalibrator")

    def __generatePhaseTerm__(self, samples, calParams, frequencies):
        """
        Parameters:
        -----------
        calParams : tuple
                A set of calibration parameters
        """

        try:
            _, timeCor, _ = calParams
        except TypeError:
            timeCor = calParams

        if self.calSettings.maxTimeCompensation is None:
            self.logger.error("No maximum compensation value was set!")
            return None

        else:
            fLow, fHigh = frequencies
            compFrequencies = np.linspace(fLow, fHigh, samples, endpoint=False)

            return np.exp(1j * compFrequencies * timeCor * 2*np.pi * self.calSettings.maxTimeCompensation)

    def generateCompensation(self, compensationVector, frequencies=None):
        """
        Calculates compensation matrix from a set of parameters

        Parameters:
        -----------
        compensationVector : list of tuples
                The parameters from which to calculate the calibration matrix
        """

        if frequencies is not None:
            self.frequencies = frequencies
            self.logger.debug("Use updated frequency vector.")

        if self.frequencies is None:
            self.logger.debug(
                "Compensation generation not possible, no frequency vector.")
            return None

        else:
            self.compensationVector = compensationVector

            compensation = np.zeros(self.size, dtype=complex)

            for compIndex, comp in enumerate(self.compensationVector):
                compensation[compIndex, :] = self.__generatePhaseTerm__(self.samples,
                                                                        comp, self.frequencies)

            self.compensation = compensation

            return self.compensation

    def applyRotation(self, rotation):
        """
        Updates calibration vector with a rotated version of the original

        Parameters:
        -----------
        rotation : double
                The rotation value in radians
        """

        self.rotation = rotation

        # Generate the new phase vector
        rotatedcompensationVector = self.originalcompensationVector + \
            np.arange(len(self.originalcompensationVector)) * \
            self.rotation * self.timeStep
        self.generateCompensation(rotatedcompensationVector)
