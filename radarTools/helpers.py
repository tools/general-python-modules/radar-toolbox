import numpy as np
import scipy.signal as sig


def dB20Mag(data):
    """
    Converts dB20-scaled data to linear
    """
    return 10**(data/20)


def dB10Mag(data):
    """
    Converts dB10-scaled data to linear
    """
    return 10**(data/10)


def mag20dB(data):
    """
    Converts linearly-scaled data to dB20
    """
    return np.log10(np.abs(data))*20


def mag10dB(data):
    """
    Converts linearly-scaled data to dB10
    """
    return np.log10(np.abs(data))*10


def getDoubleSidedScale(scale):
    return np.concatenate((np.flip(scale)[:-1], scale))


class FmcwHelper():
    def __init__(self, acqSettings, procSettings):
        """
        Initialize FMCW helper object

        Parameters
        ----------
        acqSettings : AcquisitionSettings object
        All acquisition settings, stored in an object

        procSettings : ProcessingSettings object
        All processing specific settings, stored in an object   
        """

        self.acqSettings = acqSettings
        self.procSettings = procSettings
        self.fftHelper = FftHelper(self.acqSettings)

        self.rangeBinSize = self.calcRangeBinSize()

        self.rangeScale = self.calcRangeScale()
        self.rangeScaleDoubleSided = self.getDoubleSidedScale(self.rangeScale)

        self.timeScale = self.calcTimeScale()
        self.angularWindow = self.calcAngularWindows()
        self.rangeWindow = self.calcRangeWindow()
        self.velocityWindow = self.calcVelocityWindow()
        self.angleScale = self.calcAngleScale()
        self.virtualCenterBin = self.calcVirtualCenterBin()

        try:
            self.virtualCenter = self.findRangeForBin(self.virtualCenterBin)
        except IndexError:
            # Invalid offset frequency was chosen
            self.virtualCenter = 0

    def getDoubleSidedScale(self, scale):
        return np.concatenate((-np.flip(scale)[:-1], scale))

    def calcVirtualCenterBin(self):
        """
        Returns : int
        Virtual center in bins due to frequency offset in chirp generation
        """
        try:
            totalFrequencyOffset = self.acqSettings.foffset - \
                self.acqSettings.samplingDelay*self.acqSettings.s
            return int(totalFrequencyOffset/self.acqSettings.fs*self.acqSettings.N)

        except TypeError:
            # Probably zero offset for virtual center
            return 0

    def calcTimeScale(self):
        """
        Returns : numpy.ndarray
        Time scale for the sampled signal, based on its sample rate
        """
        try:
            return np.linspace(0, self.fftHelper.Ts*self.fftHelper.M, self.fftHelper.M, endpoint=False)

        except AttributeError:
            # Settings not available for calculating window
            return None

    def findNearestRangeBin(self, rng):
        return int(np.abs(self.rangeScale - rng).argmin())

    def findNearestAngularBin(self, ang):
        return int(np.abs(self.angleScale - ang).argmin())

    def findFreqForBin(self, b):
        """
        Returns : double
        Frequency that corresponds to a specified bin
        """
        return self.fftHelper.fSingleSided[int(b)]

    def findRangeForBin(self, b):
        """
        Returns : double
        Range that corresponds to a specified bin
        """
        return self.rangeScale[int(b)]

    def calcAngleScale(self):
        """
        Returns : numpy.ndarray
        Angle scale for spectrum
        """
        try:
            # FIXME: asin() Vrtzrttung
            return self.fftHelper.angleDoubleSided

        except:
            # Settings not available for calculating angle scale
            return None

    def calcRangeBinSize(self):
        try:
            if self.acqSettings.s != 0:
                return self.acqSettings.c*self.acqSettings.fs/(self.acqSettings.s * 2 * self.acqSettings.N)
            else:
                return 1

        except:
            # Settings not available for calculating window
            return None

    def calcRangeScale(self):
        """
        Returns : numpy.ndarray
        Range scale for the spectrum after the transformation of the time domain signal
        """
        try:
            if self.acqSettings.s != 0:
                return self.acqSettings.c*self.fftHelper.fSingleSided/self.acqSettings.s/2
            else:
                return self.fftHelper.fSingleSided

        except:
            # Settings not available for calculating window
            return None

    def calcAngularWindows(self):
        """
        Returns : numpy.ndarray
        Window for use before the angular Fourier transform
        """
        try:
            return (self.fftHelper.window(self.acqSettings.Vx, self.procSettings.angularWindowType), self.fftHelper.window(self.acqSettings.Vy, self.procSettings.angularWindowType))
        
        except:
            try:
                return self.fftHelper.window(self.acqSettings.V, self.procSettings.angularWindowType)

            except:
                # Settings not available for calculating window
                return None

    def calcRangeWindow(self):
        """
        Returns : numpy.ndarray
        Window for use before the time-domain Fourier transform
        """
        try:
            return self.fftHelper.window(self.acqSettings.M, self.procSettings.rangeWindowType)

        except:
            # Settings not available for calculating window
            return None

    def calcVelocityWindow(self):
        """
        Returns : numpy.ndarray
        Window for use before the time-domain Fourier transform
        """
        try:
            return self.fftHelper.window(self.acqSettings.S, self.procSettings.velocityWindowType)

        except:
            # Settings not available for calculating window
            return None


class FftHelper():
    """
    Helps with the calculation of frequency axes and other properties of the Fourier transform
    """

    def __init__(self, settings):
        """
        Initialize FFT helper object

        Parameters
        ----------
        settings : AcquisitionSettings                                                                                                         All acquisition settings, stored in an object
        """

        self.settings = settings

        self.M = self.settings.M
        self.N = self.settings.N
        self.fs = self.settings.fs

        if self.N % 2 != 0:
            print("N must be even.")
            raise ValueError

        # Calculate sample interval
        self.Ts = 1/self.fs

        # Frequency resolution after FFT
        self.deltaF = self.fs/self.N

        # Calculate frequency scales
        self.indexDoubleSided = self.calcIndexDoubleSided()
        self.indexSingleSided = self.calcIndexSingleSided()
        self.fDoubleSided = self.calcFVectorDoubleSided()
        self.fSingleSided = self.calcFVectorSingleSided()

        # Calculate angular scale
        self.angleDoubleSided = self.calcAngleDoubleSided()

    def calcIndexDoubleSided(self):
        return np.linspace(-self.N/2+1, self.N/2, self.N, dtype=int)

    def calcIndexSingleSided(self):
        idx = self.calcIndexDoubleSided()
        return idx[int(self.N/2-1):]

    def calcFVectorDoubleSided(self):
        """
        Double sided frequency vector (-fs/2 .. fs/2)
        """
        idx = self.calcIndexDoubleSided()
        return idx*self.deltaF

    def calcFVectorSingleSided(self):
        """
        Single sided frequency vector (0 .. fs/2)
        """
        f = self.calcFVectorDoubleSided()
        return f[self.indexSingleSided+int(self.N/2-1)]

    def calcAngleDoubleSided(self):
        """
        Double sided antenna vector
        """
        if self.settings.W is not None:
            # Zero padding is used in antenna direction
            count = self.settings.W
        else:
            count = self.settings.V

        return np.linspace(-90, 90, count)

    def window(self, winLength, type):
        """
        Generates a window function, based on the desired window type and length

        Parameters
        ----------
        winLength : int
        Length of desired window

        type : str
        Type of window (default: "Nuttall")
        """

        if type is None:
            type = "Nuttall"

        if type == "Nuttall":
            return sig.windows.nuttall(winLength)

        elif type == "Hamming":
            return sig.windows.hamming(winLength)

        elif type == "Rectangular":
            return sig.windows.boxcar(winLength)

        elif type == "Flattop":
            return sig.windows.flattop(winLength)
