import numpy as np
from scipy import ndimage


class MimoPolarTransform():
    def __init__(self, outputImageShape):
        """
        Initialize MIMO helper

        Parameters
        ----------
            outputImageShape : tuple
                The desired shape of the output image in cartesian coordinates (unit is BINS, needs to be converted to meters later)
        """

        self.r = None
        self.phi = None

        self.rSpacing = None
        self.phiSpacing = None
        self.angularSectors = None
        self.distanceSectors = None

        try:
            self.xLen, self.yLen = outputImageShape
        except TypeError:
            # Half circle as default aspect ratio
            self.yLen = outputImageShape
            self.xLen = 2*self.yLen-1

    @property
    def configured(self):
        if self.r is None or self.phi is None:
            return False
        else:
            return True

    @property
    def angularSectors(self):
        return self._angularSectors

    @property
    def distanceSectors(self):
        return self._distanceSectors

    @angularSectors.setter
    def angularSectors(self, new_angularSectors):
        self._angularSectors = new_angularSectors

    @distanceSectors.setter
    def distanceSectors(self, new_distanceSectors):
        self._distanceSectors = new_distanceSectors

    def __cartToPolar(self, X, Y):
        # Transform grid of cartesian coordinates to polar, including scaling
        r = np.sqrt(X**2 + Y**2)/self.rSpacing
        phi = (1+np.sin(np.arctan2(X, Y)))*self.phiSpacing

        return (r, phi)

    def __polarToCart(self, R, PHI):
        ap = np.arcsin(PHI/self.phiSpacing-1)

        y = np.sqrt((R*self.rSpacing)**2 / (1+np.tan(ap)**2))
        x = y*np.tan(ap)

        # Output is in unit of range bins
        return (x, y)

    def calculateGrid(self, viewArea=None, resolution=800):
        if viewArea is not None \
                and self.angularSectors is not None \
                and self.distanceSectors is not None:

            # extract view area properties
            width = viewArea.width()
            height = viewArea.height()
            left = viewArea.left()
            top = viewArea.bottom()
            right = viewArea.right()
            bottom = viewArea.top()

            maxDimension = np.max([width, height])

            # Shape a selective grid of cartesian coordinates
            self.xVector = np.linspace(left, right, num=int(width /
                                                            maxDimension * resolution))
            self.yVector = np.linspace(
                bottom, top, num=int(height/maxDimension * resolution))

        else:
            # Shape a grid of cartesian coordinates
            self.xVector = np.arange(-np.floor(self.xLen/2),
                                     np.ceil(self.xLen/2))
            self.yVector = np.arange(np.round(self.yLen))

        # create meshgrid
        self.X, self.Y = np.meshgrid(self.xVector, self.yVector)

        # Calculate spacing of phi/r bins with regard to image input size
        self.phiSpacing = (self.angularSectors-1)/2
        self.rSpacing = len(self.yVector)/self.distanceSectors

        self.r, self.phi = self.__cartToPolar(self.X, self.Y)

    def transformPoints(self, rData, phiData):
        """
        Transforms scatter point data
        """
        return self.__polarToCart(rData, phiData)

    def remap(self, data):
        """
        Remaps data to a new grid

        Parameters
        ----------
            data : numpy.ndarray
                The payload data, 2D 
        """
        return ndimage.map_coordinates(data, [self.X, self.Y])

    def transform(self, data):
        """
        Perform actual interpolation of input data

        Parameters
        ----------
            data : numpy.ndarray
                The payload data, 2D 
        """
        return ndimage.map_coordinates(data, [self.r, self.phi])

    def transformAxes(self, binSize=1):
        return (self.xVector/self.rSpacing*binSize, self.yVector/self.rSpacing*binSize)
