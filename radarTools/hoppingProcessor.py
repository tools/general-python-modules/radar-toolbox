import numpy as np
from .helpers import FftHelper


class HoppingProcessor():
    """
    Class for evaluating MIMO data with frequency hopping
    """

    def __init__(self, settings, hopFrequency=None):
        """
        Initializes hopping processor.

        Parameters:
        -----------
        hopFrequency : double
            The frequency difference between two hop stages in the baseband

        settings : AcquisitionSettings
            Settings object with various settings
        """

        self.helper = FftHelper(settings)

        self.settings = settings

        if hopFrequency is None:
            self.hopFrequency = self.settings.fhop

        else:
            self.hopFrequency = hopFrequency

        # Convert hop frequency to the correct amount of bins
        hopBins = self.hopFrequency/self.helper.deltaF

        if hopBins != np.round(hopBins):
            raise ValueError(
                "Amount of hopping does not result in integer bin hop! Please correct zero padding for time domain signals")

        self.hopBins = int(hopBins)

    def rotate(self, data):
        """
        Rotates spectra to a common index

        Parameters:
        -----------
        data : np.ndarray
            Raw input data from all hops
        """
        totalHops = len(data)

        rotated = []

        # FIXME: Remove loop
        for idx, hop in enumerate(data):
            rotated.append(
                hop[:, idx*self.hopBins:-(totalHops-idx)*self.hopBins])

        return rotated

    def combine(self, data):
        """
        Combines spectra by addition of their logarithmic representations (geometric mean)

        Parameters:
        -----------
        data : np.ndarray
            Raw input data from all hops
        """
        totalHops = len(data)

        d = np.array(data)

        # Combine and normalize
        dLog = 20*np.log10(np.abs(d))/totalHops

        return np.sum(dLog, axis=0)
