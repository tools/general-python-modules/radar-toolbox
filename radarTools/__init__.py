"""
Radar tools provide helper functions and structures for evaluating raw radar data.
The "AcquisitionSettings" object shall be used for distributing metadata across all modules of this toolbox.
"""

from .fileHandler import FileHandler
from .helpers import FftHelper, FmcwHelper, mag20dB, mag10dB, dB20Mag, dB10Mag
from .acquisitionSettings import AcquisitionSettings
from .processingSettings import ProcessingSettings
from .generalSettings import GeneralSettings
from .mimoPolarTransform import MimoPolarTransform
from .iaa import Iaa
from .mimoProcessor import MimoProcessor
from .hoppingProcessor import HoppingProcessor
from .calibrator import AutoCal, TimeCalibrator, PhaseCalibrator, CalSettings, entropy, variance
from .sim import BasebandMimoSimulator, TargetPolar, TargetCartesian, SimulationSettings, ADC, SNR, Distorter, Distortion
from .cfar import Cfar, CfarSettings
from .meta import Meta, RadarContainer
from .objectStorage import ObjectStorage
from .clean import Clean

import numpy as np
