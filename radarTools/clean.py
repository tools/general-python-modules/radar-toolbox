import numpy as np

#@nb.jit
def cleanup(data, peak):
    clean = data
    z = np.zeros_like(data)
    peakShape = peak.shape

    peakShiftX = int(peakShape[0]/2)
    peakShiftY = int(peakShape[1]/2)

    for k in range(100):
        # Maximum index within the matrix
        ind = np.unravel_index(np.argmax(clean, axis=None), clean.shape)

        # Subtraction step
        clean[ind[0]-peakShiftX:ind[0]+peakShiftX, ind[1]-peakShiftY:ind[1]+peakShiftY] -= 0.8*peakShape*clean[ind]
        z[ind[0]-peakShiftX:ind[0]+peakShiftX, ind[1]-peakShiftY:ind[1]+peakShiftY] += 0.8*peakShape*clean[ind]



class Clean():
    def __init__(self):
        pass

    def work(self, data, peak):
        cleanup(data, peak)

