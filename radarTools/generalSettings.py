class GeneralSettings():
    def __init__(self, pos=None, quadrant=None, boresight=None):
        """
        Parameters:
        -----------
        pos: (float, float)
        Horizontal/vertical position of radar in decimal degrees

        quadrant: (str, str)
        Northing/easting quadrant of the radar

        boresight: float
        Boresight angle of the radar in decimal degrees
        """
        self.pos = pos
        self.quadrant = quadrant
        self.boresight = boresight
