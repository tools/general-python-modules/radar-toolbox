import pickle

class ObjectStorage():
    def __init__(self):
        pass

    def store(self, obj, filename):
        fh = open(filename, "wb")
        pickle.dump(obj, fh)
        fh.close()

    def load(self, filename):
        fh = open(filename, "rb")
        obj = pickle.load(fh)
        fh.close()

        return obj
