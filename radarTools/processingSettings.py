from typing import Iterable


class ProcessingSettings():
    """
    Stores processing related settings in an object
    """

    def __init__(self,
                 cfarType=None,
                 rangeWindowType="Nuttall",
                 angularWindowType="Nuttall",
                 velocityWindowType="Nuttall"):
        """
        cfarType : str
        Type of CFAR algorithm

        rangeWindowType : str
        The type of window, used in range direction (default: "Nuttall")

        angularWindowType : str
        The type of window, used in angular direction (default: "Nuttall")
        """

        self.cfarType = None

        self.allowedWindowTypes = ["Hamming", "Nuttall", "Rectangular"]

        self.rangeWindowType = rangeWindowType
        self.angularWindowType = angularWindowType
        self.velocityWindowType = velocityWindowType

        self.performRangeCompression = True
        self.performAngularEstimation = True
        self.performSlowTimeDetection = False

    @property
    def allowedWindowTypes(self):
        return self._allowedWindowTypes

    @allowedWindowTypes.setter
    def allowedWindowTypes(self, new_allowedWindowTypes):
        if isinstance(new_allowedWindowTypes, Iterable):
            self._allowedWindowTypes = new_allowedWindowTypes

    @property
    def rangeWindowType(self):
        return self._rangeWindowType

    @rangeWindowType.setter
    def rangeWindowType(self, new_rangeWindowType):
        if new_rangeWindowType in self.allowedWindowTypes:
            self._rangeWindowType = new_rangeWindowType

    @property
    def angularWindowType(self):
        return self._angularWindowType

    @angularWindowType.setter
    def angularWindowType(self, new_angularWindowType):
        if new_angularWindowType in self.allowedWindowTypes:
            self._angularWindowType = new_angularWindowType

    @property
    def velocityWindowType(self):
        return self._velocityWindowType

    @velocityWindowType.setter
    def velocityWindowType(self, new_velocityWindowType):
        if new_velocityWindowType in self.allowedWindowTypes:
            self._velocityWindowType = new_velocityWindowType

    def __eq__(self, other):
        # Compare CFAR type
        if self.cfarType != other.cfarType:
            return False

        # Compare range window
        if self.rangeWindowType != other.rangeWindowType:
            return False

        # Compare angular window
        if self.angularWindowType != other.angularWindowType:
            return False

        return True

    def __ne__(self, other):
        return not self.__eq__(other)
