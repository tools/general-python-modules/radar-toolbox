import glob
import natsort

def pathIsSet(path):
    """
    Checks if path is valid or not

    Parameters:
    -----------
    path : str
        The path
    """
    if path is None:
        print("No path was specified.")
        return False
    else:
        return True

class FileHandler():
    """
    Looks for files in folders and returns a list of them
    """
    def __init__(self, path = None, extension = ".pkl", recursive = True):
        """
        Initialize file handler

        Parameters:
        -----------
        path : str
            The search path for files

        extension : str
            The extension for which to look for in the given path
        
        recursive : bool
            Whether to search recursively in the path or not
        """

        self.extension = extension
        self.path = path

    def findFiles(self, path = None, recursive = True, extension = None, sorted = True):
        """
        Find files in a folder and return a list of their paths

        Parameters:
        -----------
        path : str
            The search path for files, default is the path that is set in the object.

        recursive : bool
            Whether to search recursively in the path or not, default is the setting in the object.

        extension : str
            The extension for which to look for in the given path, default is the setting in the object.

        sorted : bool
            Sort files "naturally"
        """
        
        if path is None:
            path = self.path

        if extension is None:
            extension = self.extension

        if pathIsSet(path):
            # Look for all files in directory
            if recursive:
                # Recursively list all files
                files = glob.glob("%s/**/*%s" % (path, extension), recursive = recursive)
            else:
                # Only look for files in root folder
                files = glob.glob("%s/*%s" % (path, extension))

            if sorted:
                # Sort files "naturally"
                files = natsort.natsorted(files)
            
            return files

        else:
            return []
    
