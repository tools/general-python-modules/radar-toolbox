"""
Radar simulator, generates time domain signals of targets purely in the baseband!
"""
from .mimoProcessor import MimoProcessor
from .helpers import mag20dB, dB20Mag
from scipy.interpolate import interp1d
import numpy as np

class ADC():
    def __init__(self, bits, fullScale=1):
        self.bits = bits
        self.fullScale = fullScale

    def convert(self, data):
        data[data > self.fullScale] = self.fullScale
        data[data < -self.fullScale] = -self.fullScale

        return np.floor(data/self.fullScale * 2**(self.bits-1))/(2**(self.bits-1))


class Distortion():
    def __init__(self, power, weight):
        self.power = power
        self.weight = weight


class Distorter():
    def __init__(self, distortionProfile):
        self.distortionProfile = distortionProfile

    def distort(self, data, normalization=None):
        if normalization is None:
            normalization = np.max(data)

        normalized = data/normalization

        distorted = np.zeros_like(normalized)

        for d in self.distortionProfile:
            distorted += np.power(normalized, d.power) * d.weight

        return distorted * normalization

class PhaseNoise():
    def __init__(self, offsets, levels):
        self.offsets = offsets
        self.levels = levels

        self.noiseSpectrum = interp1d(np.log10(offsets), levels, kind="cubic")
    
    def genPhaseNoiseVector(self, points, fStep):
        noise = np.random.normal(0, 1, points)
        np.fft.rfft(noise)

        np.linspace(0, points*fstep, points, endpoint = False)
        self.noiseSpectrum()
        



class SNR():
    def __init__(self, SNRdB):
        self.SNRdB = SNRdB
        self.SNRLin = 1/dB20Mag(self.SNRdB)

    def addNoise(self, data):
        data /= np.amax(data)
        noise = np.random.normal(0, self.SNRLin, data.shape)
        data += noise
        return data


class SimulationSettings():
    """
    Stores simulation settings in an object
    """

    def __init__(self, settings, SNRdB=None, adcBits=None, distortionList=None, phaseNoiseProfile=None, phaseError=False, name=None):
        """
        Initialization of simulation settings

        Parameters:
        -----------
        SNRdB : double
            Signal to noise ratio in the baseband. Target amplitudes are considered relative. The baseband SNR is then established, based on the normalized noise-free baseband signal.
        """
        self.acqSettings = settings
        self.SNRdB = SNRdB
        self.adcBits = adcBits
        self.phaseNoiseProfile = phaseNoiseProfile
        self.distortionList = distortionList

        if phaseError is False:
            self.phaseErrorVector = None

        elif phaseError is True:
            self.phaseErrorVector = self.__generatePhaseDeviations(
                2*np.pi)

        else:
            self.phaseErrorVector = phaseError

        print("Starting phase error vector is")
        print(self.phaseErrorVector)

        self.name = name

    def __generatePhaseDeviations(self, phaseError):
        return np.mod(np.random.normal(0, phaseError, self.acqSettings.V), np.pi)


class TargetPolar():
    """
    Wrapper type for target objects
    """

    def __init__(self, distance, bearing, amplitude):
        """
        Parameters:
        -----------
        distance : double
            Distance of the target in m

        bearing : double
            Bearing of the target in degrees

        amplitude : double
            Relative target amplitude
        """
        self.distance = distance
        self.bearing = bearing
        self.amplitude = amplitude


class TargetCartesian():
    """
    Wrapper type for target objects
    """

    def __init__(self, x, y, amplitude):
        """
        Parameters:
        -----------
        distance : double
            Distance of the target in m

        bearing : double
            Bearing of the target in degrees

        amplitude : double
            Relative target amplitude
        """
        self.distance = np.sqrt(np.square(x)+np.square(y))
        self.bearing = 180*np.arctan2(x, y)/np.pi
        self.amplitude = amplitude


class BasebandMimoSimulator():
    """
    Baseband simulator for FMCW MIMO systems
    """

    def __init__(self, acquisitionSettings, simulationSettings, processingSettings):
        """
        Parameters:
        -----------
        acquisitionSettings : AcquisitionSettings object
            Stores all system related settings

        simulationSettings : SimulationSettings object
            Stores all simulation related settings 

        processingSettings : ProcessingSettings object
            Stores processing related settings 
        """

        # Initialize necessary objects
        self.simulationSettings = simulationSettings
        self.acquisitionSettings = acquisitionSettings
        self.processingSettings = processingSettings
        self.processor = MimoProcessor(
            self.acquisitionSettings, self.processingSettings)
        self.sigGen = BasebandSignalGenerator(
            self.acquisitionSettings, self.simulationSettings, self.processor)

        if self.simulationSettings.adcBits is not None:
            self.adc = ADC(self.simulationSettings.adcBits)
        else:
            self.adc = None

        if self.simulationSettings.SNRdB is not None:
            self.snr = SNR(self.simulationSettings.SNRdB)
        else:
            self.snr = None

        if self.simulationSettings.distortionList is not None:
            self.distorter = Distorter(self.simulationSettings.distortionList)
        else:
            self.distorter = None

        if self.simulationSettings.phaseNoiseProfile is not None:
            self.phaseNoise = PhaseNoise(self.simulationSettings.phaseNoiseProfile)
        else:
            self.phaseNoise = None

    def generateBasebandSignals(self, targets):
        """
        Generate baseband signals for all virtual antennas and add noise

        Parameters:
        -----------
        targets : list of Target objects
            List of targets
        """

        # Generates noise-free baseband signals for all targets and virtual antennas
        bbSig = self.sigGen.generateAll(targets)

        if self.snr is not None:
            bbSig = self.snr.addNoise(bbSig)

        if self.adc is not None:
            bbSig = self.adc.convert(bbSig)

        if self.distorter is not None:
            bbSig = self.distorter.distort(bbSig)

        return bbSig


class BasebandSignalGenerator():
    """
    The actual baseband signal generating class
    """

    def __init__(self, acqSettings, simSettings, processor):
        """
        Parameters:
        -----------
        acqSettings : AcquisitionSettings object
            Stores all system related settings

        processor : MimoProcessor object
            Used for calculating system related properties
        """
        self.acqSettings = acqSettings
        self.simSettings = simSettings
        self.processor = processor
        self.fmcwHelper = self.processor.fmcwHelper
        self.fftHelper = self.fmcwHelper.fftHelper

    def generateSingle(self, target, phaseError=0, antennaIndex=0):
        """
        Generate a single baseband signal based on a specific target and antenna index

        Parameters:
        -----------
        target : Target object
            Holds target information

        antennaIndex : int
            The antenna in the array for which the baseband signal is currently calculated (goes from 0 to V-1, where V is the number of virtual elements)
        """
        # Calculate baseband frequency of target
        fb = self.acqSettings.s/self.acqSettings.c * target.distance*2

        # Calculate phase offset for this antenna
        phaseOffset = self.processor.getAntennaPhaseProgression(
            target.bearing, deg=True)*antennaIndex

        # Get time scale for the given settings
        t = self.fmcwHelper.timeScale

        # Calculate time domain signal
        tds = target.amplitude*np.sin(2*np.pi*fb*t + phaseOffset + phaseError)

        return tds

    def generateAll(self, targets):
        """
        Generates the combined baseband signals for all targets and virtual antenna elements.

        Parameters:
        -----------
        targets : list of Target objects
            Contains information about all targets that are simulated
        """

        # Reserve space
        timeDomainSignals = np.zeros(
            (self.acqSettings.V, len(self.fmcwHelper.timeScale)))

        # Iterate over all targets
        for target in targets:
            # Iterate over all virtual antenna elements
            if self.simSettings.phaseErrorVector is not None:
                for vIdx, phaseError in enumerate(self.simSettings.phaseErrorVector):
                    timeDomainSignals[vIdx, :] += self.generateSingle(
                        target, phaseError=phaseError, antennaIndex=vIdx)

            else:
                for vIdx in range(self.acqSettings.V):
                    timeDomainSignals[vIdx,:] += self.generateSingle(target, antennaIndex=vIdx)

        # Return normalized time domain data
        return timeDomainSignals/len(targets)
