import datetime
import time
import uuid

class RadarContainer():
    def __init__(self, meta, settings, data):
        self.meta = meta
        self.settings = settings
        self.data = data

        # Radar image data
        self.image = None

        # Radar target data
        self.targets = None

        # If fresh == True, the radarContainer was recently processed and contains valid results
        self.fresh = False


class Meta():
    """
    Storage class for auxiliary data
    """

    def __init__(self, names):
        """
        Parameters:
        -----------
        names : list of str
        Names of platforms

        """

        # Generates a unique ID for the radar container
        self.uniqueId = uuid.uuid4()
        self.names = names

        # Generate timestamp at creation
        self.timestamp = time.time()
        self.timeOfBirth = datetime.datetime.fromtimestamp(self.timestamp)
        self.timeOfBirthStr = str(self.timeOfBirth)
