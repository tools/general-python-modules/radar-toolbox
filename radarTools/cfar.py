import numpy as np
import numba as nb
import logging


@nb.jit
def caDetector(rowIdx, colIdx, dataset, rowTestIndices, colTestIndices, targetRow, targetCol, thresholdFactor):
    """
    Detector function that works based on cell averaging CFAR

    Parameters:
    -----------
    rowIdx/colIdx : int
        The row/column of the current cell under test within the dataset. This row is the first row of the cell (upmost), the column is the first column of the cell (leftmost).

    dataset : np.ndarray
        The full image

    rowTestIndices/colTestIndices : np.ndarray
        The relative indices in the cell which are actually evaulated (rows and columns)

    targetRow/targetCol : int
        The relative index in the cell, where the target is located.

    thresholdFactor : double
        Multiplicator for the calculated detection threshold for finding out the final threshold.
    """

    # Calculate the final indices of the bins that are used for detection
    fullRowIdx = rowTestIndices+rowIdx
    fullColIdx = colTestIndices+colIdx
    dataFrame = dataset[fullRowIdx, fullColIdx]

    meanDataFrame = np.mean(dataFrame)

    detection = meanDataFrame * \
        thresholdFactor < dataset[rowIdx+targetRow, colIdx+targetCol]

    if detection:
        return dataset[rowIdx+targetRow, colIdx+targetCol]
    else:
        return 0


@nb.jit
def lineOsDetector(rowIdx, colIdx, dataset, targetRow, thresholdLevel, thresholdFactor, sortedDataFrame):
    """
    Detector function that works based on OS CFAR but does not use a window but adjacent range bins entirely

    Parameters:
    -----------
    rowIdx/colIdx : int
        The row/column of the current cell under test within the dataset. This row is the first row of the cell (upmost), the column is the first column of the cell (leftmost).

    dataset : np.ndarray
        The full image

    targetRow : int
        The relative row index in the cell, where the target is located.

    thresholdLevel : int
        The Nth element out of the list of test elements is chosen as the threshold value. After multiplication with the thresholdFactor, the final threshold is calculated.

    thresholdFactor : double
        Multiplicator for the calculated detection threshold for finding out the final threshold.
    """
    detection = sortedDataFrame[thresholdLevel] * \
        thresholdFactor < dataset[rowIdx+targetRow, colIdx]

    if detection:
        return dataset[rowIdx+targetRow, colIdx]
    else:
        return 0


@nb.jit
def osDetector(rowIdx, colIdx, dataset, rowTestIndices, colTestIndices, targetRow, targetCol, thresholdLevel, thresholdFactor):
    """
    Detector function that works based on OS CFAR

    Parameters:
    -----------
    rowIdx/colIdx : int
        The row/column of the current cell under test within the dataset. This row is the first row of the cell (upmost), the column is the first column of the cell (leftmost).

    dataset : np.ndarray
        The full image

    rowTestIndices/colTestIndices : np.ndarray
        The relative indices in the cell which are actually evaulated (rows and columns)

    targetRow/targetCol : int
        The relative index in the cell, where the target is located.

    thresholdLevel : int
        The Nth element out of the list of test elements is chosen as the threshold value. After multiplication with the thresholdFactor, the final threshold is calculated.

    thresholdFactor : double
        Multiplicator for the calculated detection threshold for finding out the final threshold.
    """
    fullRowIdx = rowTestIndices+rowIdx
    fullColIdx = colTestIndices+colIdx
    dataFrame = dataset[fullRowIdx, fullColIdx]

    sortedDataFrame = np.sort(dataFrame)

    detection = sortedDataFrame[thresholdLevel] * \
        thresholdFactor < dataset[rowIdx+targetRow, colIdx+targetCol]

    if detection:
        return dataset[rowIdx+targetRow, colIdx+targetCol]
    else:
        return 0


def cfarLoopLineOS(dataset, settings):
    """
    Loop functionality for the CFAR algorithm (ordered statistics)
    This uses line test regions instead of windows
    Calls the evaluator function on all elements in the dataset

    Parameters:
    -----------
    dataset : np.ndarray
        The full image for processing

    settings : Settings object
        All settings for CFAR evaluation
    """
    rows, cols = dataset.shape
    thresholdFactor = settings.thresholdFactor
    thresholdLevel = settings.lineThresholdLevel
    rowTestIndices, colTestIndices = settings.lineIndexMask
    targetRow = settings.targetRow

    targets = np.zeros_like(dataset)
    for rowIdx in nb.prange(rows-settings.totalCellHeight+settings.testBinsVertical):

        # Calculate sorted data frame only once per row.
        fullRowIdx = rowTestIndices+rowIdx
        dataFrame = dataset[fullRowIdx, colTestIndices]
        sortedDataFrame = np.sort(dataFrame)

        for colIdx in nb.prange(cols):
            targets[rowIdx+targetRow, colIdx] = lineOsDetector(
                rowIdx, colIdx, dataset, targetRow, thresholdLevel, thresholdFactor, sortedDataFrame)

    det = np.nonzero(targets)

    return det, targets[det]


@nb.jit
def cfarLoopOS(dataset, settings):
    """
    Loop functionality for the CFAR algorithm (ordered statistics)
    Calls the evaluator function on all elements in the dataset

    Parameters:
    -----------
    dataset : np.ndarray
        The full image for processing

    settings : Settings object
        All settings for CFAR evaluation
    """
    rows, cols = dataset.shape
    thresholdFactor = settings.thresholdFactor
    thresholdLevel = settings.thresholdLevel
    rowTestIndices, colTestIndices = settings.indexMask
    targetRow = settings.targetRow
    targetCol = settings.targetCol

    targets = np.zeros_like(dataset)
    for rowIdx in nb.prange(rows-settings.totalCellHeight+settings.testBinsVertical):
        for colIdx in nb.prange(cols-settings.totalCellWidth+settings.testBinsHorizontal):
            targets[rowIdx+targetRow, colIdx+targetCol] = osDetector(
                rowIdx, colIdx, dataset, rowTestIndices, colTestIndices, targetRow, targetCol, thresholdLevel, thresholdFactor)

    det = np.nonzero(targets)

    return det, targets[det]


@nb.jit
def cfarLoopCA(dataset, settings):
    """
    Loop functionality for the CFAR algorithm (cell averaging)
    Calls the evaluator function on all elements in the dataset

    Parameters:
    -----------
    dataset : np.ndarray
        The full image for processing

    settings : Settings object
        All settings for CFAR evaluation
    """
    rows, cols = dataset.shape
    thresholdFactor = settings.thresholdFactor
    rowTestIndices, colTestIndices = settings.indexMask
    targetRow = settings.targetRow
    targetCol = settings.targetCol

    targets = np.zeros_like(dataset)
    for rowIdx in nb.prange(rows-settings.totalCellHeight+settings.testBinsVertical):
        for colIdx in nb.prange(cols-settings.totalCellWidth+settings.testBinsHorizontal):
            targets[rowIdx+targetRow, colIdx+targetCol] = caDetector(
                rowIdx, colIdx, dataset, rowTestIndices, colTestIndices, targetRow, targetCol, thresholdFactor)

    det = np.nonzero(targets)

    return det, targets[det]


class CfarSettings():
    """
    CFAR settings storage
    """

    def __init__(self, guardBinsHorizontal=3, guardBinsVertical=25, thresholdFactor=1, threshold=0.8, mode=None, searchArea=None, enabled=False):
        self.enabled = enabled
        self.searchArea = searchArea
        self.mode = mode

        # Shape of test bin area in the middle of cell. Keep at one!
        self.testBinsHorizontal = 1
        self.testBinsVertical = 1

        # Guard bin size around the test bins
        self.guardBinsHorizontal = guardBinsHorizontal
        self.guardBinsVertical = guardBinsVertical

        # Size of estimation bins at the perimeter of the cell
        self.estimationBinsHorizontal = 1
        self.estimationBinsVertical = 1

        self.thresholdFactor = thresholdFactor
        self.threshold = threshold

        # Calculate total cell size
        self.totalCellWidth = self.testBinsHorizontal + \
            self.guardBinsHorizontal*2 + self.estimationBinsHorizontal*2
        self.totalCellHeight = self.testBinsVertical + \
            self.guardBinsVertical*2 + self.estimationBinsVertical*2

        self.mask = np.ones((self.totalCellHeight, self.totalCellWidth))
        self.mask[self.testBinsHorizontal:-self.testBinsHorizontal,
                  self.testBinsVertical:-self.testBinsVertical] = 0

        # Target position
        self.targetCol = self.guardBinsHorizontal + self.estimationBinsHorizontal
        self.targetRow = self.guardBinsVertical + self.estimationBinsVertical

        # Indices of nonzero elements in the array
        self.indexMask = np.nonzero(self.mask)

        # Calculate threshold bin
        self.thresholdLevel = int(
            np.round(self.indexMask[0].size * self.threshold))

    def calcLineMask(self, dataColumns):
        self.dataColumns = dataColumns

        self.lineMask = np.ones((self.totalCellHeight, dataColumns))
        self.lineMask[self.testBinsHorizontal:-self.testBinsHorizontal, :] = 0

        # Indices for line OS CFAR
        self.lineIndexMask = np.nonzero(self.lineMask)

        self.lineThresholdLevel = int(
            np.round(self.lineIndexMask[0].size * self.threshold))


class Cfar():
    """
    Wrapper class for CFAR calculation functionality
    """

    def __init__(self, settings=None, loggerName=None):
        """
        CFAR initialization function

        Parameters:
        -----------
        settings : Settings object
            Storage of all CFAR settings. If not supplied, a local settings object is created with default settings.
        """

        if loggerName is None:
            loggerName = "general_logs"

        self.logger = logging.getLogger("%s.Cfar" % loggerName)

        if settings is None:
            self.settings = CfarSettings()
        else:
            self.settings = settings
    
    def setup(self, settings):
        self.settings = settings
    
    def findTargets(self, dataset):
        """
        Target search function.

        Parameters:
        -----------
        dataset : np.ndarray
            The full image
        """

        try:
            # Limit search area in the dataset to a certain range
            dataset = dataset[self.settings.searchArea[0]:self.settings.searchArea[1], :]
        except TypeError:
            self.logger.debug("Use full array for CFAR detection.")
        else:
            self.logger.debug("Use limited search area for CFAR.")

        mode = self.settings.mode

        if mode is None:
            mode = "Line OS"

        if mode == "Line OS":
            dataColumns = dataset.shape[1]

            try:
                if self.settings.dataColumns != dataColumns:
                    self.settings.calcLineMask(dataColumns)
            except AttributeError:
                self.settings.calcLineMask(dataColumns)

            targets, amplitudes = cfarLoopLineOS(dataset, self.settings)

        if mode == "OS":
            targets, amplitudes = cfarLoopOS(dataset, self.settings)

        if mode == "CA":
            targets, amplitudes = cfarLoopCA(dataset, self.settings)

        try:
            # Correct indices in distance due to limited search area.
            targets[0] += self.settings.searchArea[0]
        except TypeError:
            pass

        r, phi = targets

        return (r, phi, amplitudes)
