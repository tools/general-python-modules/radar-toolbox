import numpy as np
import scipy.io as sio
import time
from scipy import signal
from numba import jit
import numba as nb


@jit
def iaa_iteration(rSize, tSize, beta, B, RZero, fmcw_fft, iaa_iter):
    """
    Perform one single IAA iteration
    """
    BETA2 = np.square(np.abs(beta))
    Bcon = np.conj(B)

    for _ in nb.prange(0, iaa_iter):
        R = RZero

        #t = time.time()
        for index_r in nb.prange(0, rSize):
            for index_theta in nb.prange(0, tSize):
                R = R + BETA2[index_r, index_theta] * \
                    np.outer(B[:, index_theta],
                             Bcon[:, index_theta])  # ok
        #print("t1: %f" % (time.time()-t))

        #t = time.time()
        Rinv = np.linalg.inv(R)
        #print("t2: %f" % (time.time()-t))

        #t = time.time()
        for index_r in nb.prange(0, rSize):
            for index_theta in nb.prange(0, tSize):
                BDotR = np.dot(Bcon[:, index_theta], Rinv)

                beta[index_r, index_theta] = np.dot(BDotR, fmcw_fft[:, index_r])/(
                    np.dot(BDotR, B[:, index_theta]))

        #print("t3: %f" % (time.time()-t))

    return beta


class IaaSettings():
    def __init__(self, steps, granularity, searchArea):
        """
        Parameters:
        -----------
        steps : int
        Number of iteration steps

        granularity : (int, int)
        Range/angular grid steps

        searchArea : ((minRange, maxRange), (minAngle, maxAngle))
        Search area of IAA algorithm
        """
        self.steps = steps
        self.granularity = granularity

        self.granularityRange = self.granularity[0]
        self.granularityAngle = self.granularity[1]

        self.searchArea = searchArea

        self.searchAreaRange = self.searchArea[0]
        self.searchAreaAngle = self.searchArea[1]

        self.minAngle = self.searchAreaAngle[0]
        self.maxAngle = self.searchAreaAngle[1]

        self.minRange = self.searchAreaRange[0]
        self.maxRange = self.searchAreaRange[1]


class Iaa():
    def __init__(self, acqSettings, fmcwHelper, iaaSettings=None):
        self.acqSettings = acqSettings
        self.fmcwHelper = fmcwHelper

        if iaaSettings is not None:
            self.iaaSettings = iaaSettings
        else:
            self.iaaSettings = IaaSettings(
                3, (100, 320), ((0, 2000), (-80, 80))
            )

    def run(self, dataIn):
        """
        Parameters:
        -----------
        data : np.ndarray
        Radar data after range compression (1st FFT)
        """
        vcenter = self.fmcwHelper.virtualCenterBin

        data = np.squeeze(dataIn[:, 0, vcenter:])

        # calculate angular and range vectors
        theta = (np.linspace(self.iaaSettings.minAngle,
                             self.iaaSettings.maxAngle, num=self.iaaSettings.granularityAngle))/180*np.pi

        r = np.linspace(self.iaaSettings.minRange,
                        self.iaaSettings.maxRange, self.iaaSettings.granularityRange)

        # generate virtual antenna array
        VX = (1+np.arange(0, self.acqSettings.V))/2

        # calculation of antenna transfer vector for every incident angle for the virtual array
        B = np.zeros((self.acqSettings.V, theta.size), dtype=complex)
        for index_theta in range(0, theta.size, 1):
            alpha = np.exp(1j*2*np.pi * np.sin(theta[index_theta])*VX)
            B[:, index_theta] = alpha

        # Delay-and-Sum (initialization step)
        # TODO: test window
        beta = np.zeros((r.size, theta.size), dtype=complex)

        # TODO: fix index steps
        for index_r in range(0, r.size):
            for index_theta in range(0, theta.size):
                beta[index_r, index_theta] = np.dot(np.conj(
                    B[:, index_theta]), data[:, index_r]) / np.dot(np.conj(B[:, index_theta]), B[:, index_theta])  # ok

        RZero = np.zeros(
            (self.acqSettings.V, self.acqSettings.V), dtype=complex
        )

        beta = iaa_iteration(r.size, theta.size, beta, B,
                             RZero, data, self.iaaSettings.steps)

        return (r, theta, beta)
