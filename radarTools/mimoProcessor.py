import numpy as np
import logging
from radarTools import FmcwHelper, Iaa
from time import time


class MimoProcessor():
    def __init__(self, acqSettings, procSettings, iaaSettings=None):
        """
        Initialize MIMO processor

        Parameters
        ----------
        acqSettings : AcquisitionSettings object
        All acquisition settings, stored in an object

        procSettings : ProcessingSettings object
        All processing specific settings, stored in an object   
        """

        self.acqSettings = acqSettings
        self.procSettings = procSettings

        self.fmcwHelper = FmcwHelper(self.acqSettings, self.procSettings)
        self.logger = logging.getLogger("general_logs.MimoProcessor")

        if iaaSettings is not None:
            self.iaa = Iaa(self.acqSettings, self.fmcwHelper,
                           iaaSettings=iaaSettings)

    def getAntennaPhaseProgression(self, bearing, deg=False):
        """
        Calculates the time domain signal phase shift from one antenna to the next for a given bearing. This assumes a target in the far field and a low relative bandwidth.

        Parameters:
        -----------
        bearing : double
            The relative angle between the antenna array and the target

        deg: bool
            Switch between radian and degrees for units. If true, supply bearing in degrees and get phase progression in degrees
        """

        if deg:
            bearing = np.radians(bearing)

        d = self.acqSettings.antennaDistance

        deltaPhase = 2*np.pi*d * \
            np.sin(bearing) / self.acqSettings.lambdaMean

        return deltaPhase

    def pad(self, data, N, axis=-1):
        """
        Zero-phase zero padding on input data

        Parameters
        ----------
        data : numpy.ndarray
            The input data

        N : int
            The length after zero padding

        axis : int
            The axis to which zero padding is applied
        """

        shape = data.shape
        dataLen = shape[axis]

        pad = np.array([np.floor((N-dataLen)/2), np.ceil((N-dataLen)/2)])

        padsArray = np.zeros((len(shape), 2))
        padsArray[axis] = pad
        padsArray = padsArray.astype(int)

        pads = tuple(map(tuple, padsArray))

        out = np.pad(data, pads, 'constant')

        rollAmount = int(N/2)
        padded = np.roll(out, rollAmount, axis=1)

        return padded

    def hopCompensate(self, data, step, getIndividualRotatedSpectra=False):
        """
        Compensate a radar image for harmonics (hopping)

        Parameters
        ----------
        data : numpy.ndarray
            Input radar image (range bins, hops, angular bins)

        step : int
            Step size in bins between each two hops
        """

        _, hops, _ = data.shape

        if hops == 1:
            return data[:, 0, :]

        else:
            hopdata = np.zeros_like(data)

            for h in range(hops):
                hopdata[:-step*hops, h, :] = data[step*h:-step*(hops-h), h, :]

            combined = np.power(np.prod(hopdata, 1), 1/hops)

            if getIndividualRotatedSpectra:
                return (combined, hopdata)
            else:
                return combined

    def calcSpectrum(self, data, window, fn, padLength=None, axis=-1):
        """
        Calculate range FFT on inputs

        Parameters
        ----------
        data : numpy.ndarray
            The input data

        padLength : int
            The length of the processed data axis after zero padding

        axis : int
            The axis on which the spectrum is calculated
        """

        # Apply window to radar data
        windowed = np.multiply(data, window)

        # Apply zero-phase zero padding

        ref = time()
        if (padLength is not None) and (padLength != data.shape[-1]) and (padLength > data.shape[-1]):
            padded = self.pad(windowed, padLength, axis)
        else:
            padded = windowed

        # self.logger.debug("Padding took %d ms" % int(1000*(time()-ref)))

        # Apply transformation function
        spectrum = fn(padded)

        return spectrum

    def calcRangeSpectrum(self, data, axis=-1):
        """
        Calculate spectrum on time domain signals (range evaluation)

        Parameters
        ----------
        data : numpy.ndarray
            The input data
        """
        return self.calcSpectrum(data, self.fmcwHelper.rangeWindow, np.fft.rfft, padLength=self.acqSettings.N, axis=axis)

    def calcVelocitySpectrum(self, data, velocityAxis=-3, axis=-1):
        """
        Calculate spectrum on time progression (slow time evaluation)

        Parameters
        ----------
        data : numpy.ndarray
            The input data
        """

        # Prepare data shape
        try:
            data = np.swapaxes(data, velocityAxis, axis)
        except TypeError:
            pass

        return self.calcSpectrum(data, self.fmcwHelper.velocityWindow, np.fft.fft, axis=axis)

    def calcAngularSpectrum(self, data, angularAxis=-2, axis=-1):
        """
        Calculate spectrum in antenna direction (angular evaluation)

        Parameters
        ----------
        data : numpy.ndarray
            The input data
        """

        # Prepare data shape
        try:
            data = np.swapaxes(data, angularAxis, axis)
        except TypeError:
            pass

        return np.fft.fftshift(self.calcSpectrum(data, self.fmcwHelper.angularWindow, np.fft.fft, padLength=self.acqSettings.W, axis=axis), axes=axis)

    def calc2dAngularSpectrum(self, data, angularAxisX = -3, angularAxisY=-2, axis=-1):
        """
        Calculate 2D angular spectrum in antenna direction (angular evaluation)

        Parameters
        ----------
        data : numpy.ndarray
            The input data
        """

        windowedx = np.multiply(data.swapaxes(angularAxisX, axis), self.fmcwHelper.angularWindow[0]).swapaxes(angularAxisX, axis)
        windowedxy = np.multiply(windowedx.swapaxes(angularAxisY, axis), self.fmcwHelper.angularWindow[1]).swapaxes(angularAxisY, axis)

        return np.fft.fftshift(np.fft.fft2(windowedxy, s=[32,32], axes=(angularAxisX, angularAxisY)), axes=(angularAxisX, angularAxisY))