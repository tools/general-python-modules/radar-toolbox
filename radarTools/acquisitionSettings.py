from warnings import warn


class AcquisitionSettings():
    """
    Stores acquisition (system) settings in an object
    """

    def __init__(self,
                 c=None,
                 M=None,
                 fs=None,
                 samplingDelay=None,
                 bits=None,
                 fhop=None,
                 foffset=None,
                 B=None,
                 T=None,
                 N=None,
                 W=None,
                 S=None,
                 NTx=None,
                 NRx=None,
                 NTxPerPlatform=None,
                 NRxPerPlatform=None,
                 fc=None,
                 trigRate=None,
                 burst=None):
        """
        Initialize acquisition settings

        Parameters
        ----------
        c : double
        The propagation speed of the radar wave

        M : int
        The amount of sampling points

        fs : double
        The sampling rate of the ADC in Hz

        samplingDelay : double
        The delay of the sampling start in s after the measurement was triggered

        bits : int
        ADC bit width

        fc : double
        The carrier frequency of the chirp signal in Hz

        fHop : double
        Hopping step frequency

        fOffset : double
        Initial offset frequency

        B : double
        The bandwidth of the chirp

        T : double
        The duration of the chirp

        N : int
        The amount of sampling points after time-domain zero padding

        W : int
        The amount of sampling points in antenna direction after zero padding

        NTx : int
        The amount of transmitter platforms

        NRx : int
        The amount of receiver platforms

        NTxPerPlatform : int
        The amount of transmitters per platform

        NRxPerPlatform : int
        The amount of receivers per platform

        trigRate : double
        The trigger rate in Hz

        burst : int
        The number of measurements per burst (e.g. for hopping)
        """

        # Store the speed of propagation
        if c is None:
            # default to the speed of light
            self.c = 3e8
        else:
            self.c = c

        # Store the speed of propagation
        if samplingDelay is None:
            # Assume that sampling was not delayed
            self.samplingDelay = 0
        else:
            self.samplingDelay = samplingDelay

        if fs is None:
            # Default sample rate
            self.fs = 20e6
        else:
            self.fs = fs

        self.fc = fc
        self.B = B
        self.T = T
        self.bits = bits
        self.S = S

        if fhop is not None:
            self.fhop = fhop
        else:
            self.fhop = 0

        if foffset is not None:
            self.foffset = foffset
        else:
            self.foffset = 0

        # ADC bit format string for settings TCP receive data format in TCP handler
        if self.bits == 16:
            self.bitsFormatString = "int16"
        elif self.bits == 8:
            self.bitsFormatString = "int8"

        try:
            # Wavelength of the highest/lowest frequency that occurs in the chirp
            self.lambdaMin = self.c / (self.fc + self.B)
            self.lambdaMax = self.c / self.fc
            self.lambdaMean = (self.lambdaMax + self.lambdaMin)/2

            # Minimum/Maximum time period during the chirp
            self.TMin = self.lambdaMin / self.c
            self.TMax = self.lambdaMax / self.c

            # Derive antenna distance from that wavelength
            self.antennaDistance = self.lambdaMax / 2
        except:
            pass

        # Calculate and store the chirp slope
        try:
            self.s = self.B / self.T
        except:
            # No slope calculation in case of exception.
            self.s = 1
            warn("Error in calculating chirp slope. Were the parameters submitted?")

        if NTx is not None:
            self.NTx = NTx
        else:
            self.NTx = 1

        if NRx is not None:
            self.NRx = NRx
        else:
            self.NRx = 1

        if NTxPerPlatform is not None:
            self.NTxPerPlatform = NTxPerPlatform
        else:
            self.NTxPerPlatform = 1

        if NRxPerPlatform is not None:
            self.NRxPerPlatform = NRxPerPlatform
        else:
            NRxPerPlatform = 1

        self.V = self.NTx * self.NRx

        if W is not None:

            # Only apply zero padding value, if larger than amount of virtual antennas (sanity check)
            if W > self.V:
                self.W = W
            else:
                self.W = self.V

        else:
            self.W = self.V

        # Calculate number of sample points, if not provided
        if M is None:
            try:
                self.M = self.fs * self.T
            except:
                self.M = None
        else:
            self.M = M

        # If N == M, no zero padding is applied
        if N is not None:
            self.N = N
        else:
            self.N = self.M

        if burst is not None:
            self.burst = burst
        else:
            self.burst = 1

        if trigRate is not None:
            self.trigRate = trigRate
        else:
            # Default trigger rate of 1 Hz
            self.trigRate = 1

    @property
    def M(self):
        return self._M

    @M.setter
    def M(self, new_M):
        int_new_M = int(new_M)

        if new_M != new_M:
            warn(
                "M changed after integer conversion: {%f}->{%d}".format(new_M, int_new_M))

        self._M = int_new_M

    def __eq__(self, other):
        # Compare virtual antenna count
        if self.V != other.V:
            return False

        if self.W != other.W:
            return False

        # Compare chirp parameters
        if self.B != other.B:
            return False

        if self.T != other.T:
            return False

        if self.fs != other.fs:
            return False

        if self.foffset != other.foffset:
            return False

        if self.fhop != other.fhop:
            return False

        if self.fs != other.fs:
            return False

        if self.burst != other.burst:
            return False

        return True

    def __ne__(self, other):
        return not self.__eq__(other)
